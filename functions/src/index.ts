import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
 
admin.initializeApp(functions.config().firebase);

export const healtcheck = functions.https.onRequest((request, response) => {
 functions.logger.info("Functions OK - Version 1.0.1");
 response.send("Firebase Functions is working OK!! - Version 1.0.1");
});

exports.addMessage = functions.https.onRequest(async (req, res) => {
      const original = req.query.text;
      const writeResult = await admin.firestore().collection('messages').add({original: original});
      res.json({result: `Message with ID: ${writeResult.id} added.`});
    });

exports.makeUppercase = functions.firestore.document('/messages/{documentId}')
    .onCreate((snap, context) => {
      const original = snap.data().original;
      functions.logger.log('Uppercasing', context.params.documentId, original);
      
      const uppercase = original.toUpperCase();
      return snap.ref.set({uppercase}, {merge: true});
    });
